CONTENTS OF THIS FILE
---------------------

  * Summary
  * Requirements
  * Facebook Configuration

SUMMARY
-------

Facebook Importer is a developer only module that integrate Facebook´s API in 
Drupal. This module allows you to authenticate with this API and
use this authentication to Import Facebook Data in JSON.

REQUIREMENTS
------------

1. Social Media Importer Module - This modules depends on Social Media Importer
 Module. 

FACEBOOK CONFIGURATION
----------------------
1. Visit https://developers.facebook.com/apps

2. Create a new Application with appropriate details,
   if you don't have an Application created.
   
3. Add your domain in App Domains field.

4. Copy the Application id, Application secret
   to add Application form of the module.
