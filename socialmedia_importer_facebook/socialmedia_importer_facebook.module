<?php
/**
 * @file
 * Import Data from Social Media Facebook in JSON.
 */

/**
 * Implements hook_cron().
 */
function socialmedia_importer_facebook_cron() {
  $application_ids = socialmedia_importer_facebook_get_applications_id_list();
  if (count($application_ids) > 0) {
    foreach ($application_ids as $application_id) {
      $facebook_application = new SocialMediaImporterFacebookApplication($application_id);
      if ($facebook_application->isAuthorized && !is_null($facebook_application->accessToken)) {
        $facebook_application->validateAccessToken();
      }
    }
  }
}

/**
 * Implements hook_socialmedia_importer_application_info().
 */
function socialmedia_importer_facebook_socialmedia_importer_application_info() {
  $info = array(
    'facebook' => array(
      'class' => 'SocialMediaImporterFacebookApplication',
      'name' => 'Facebook',
      'description' => t("A Facebook Application is required to access 
          Facebook's API. Use your Application name, id and secret to add a 
          Facebook Application. If you do not have a Facebook Application, 
          you can create one at: !url.",
          array('!url' => l(t('https://developers.facebook.com/apps'), 'https://developers.facebook.com/apps'))),
    ),
  );
  return $info;
}

/**
 * Implements hook_form_alter().
 */
function socialmedia_importer_facebook_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'socialmedia_importer_ui_add_application_form';
      if ($form['provider']['#value'] == 'facebook') {
        $form['app_id']['#title'] = t('Application Id');
        $form['app_id']['#description'] = t('Enter your facebook Application id.
            This can be found in the settings of your Facebook Application Details page: !url',
            array('!url' => l(t('https://developers.facebook.com/apps'), 'https://developers.facebook.com/apps')));
        $form['app_secret']['#title'] = t('Application Secret');
        $form['app_secret']['#description'] = t('Enter your facebook Application secret.
            This can be found in the settings of your Facebook Application 
            Details page: !url.',
            array('!url' => l(t('https://developers.facebook.com/apps'), 'https://developers.facebook.com/apps')));
      }
      break;

  }
  return $form;
}

/**
 * Implements hook_help().
 */
function socialmedia_importer_facebook_help($path, $arg) {
  switch ($path) {
    case 'admin/help#socialmedia_importer_facebook':
      $module_path = $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'socialmedia_importer_facebook');
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Facebook Importer is a developer only module that integrate Facebook´s
          API in Drupal.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<p>' . t('This module allows you to authenticate with this API and
          use this authentication to Import Facebook Data in JSON') . '</p>';
      $output .= '<p>' . t("For more details read the
          <a href='@url'>README.txt</a> file in the Facebook Importer
          module directory.", array('@url' => "$module_path/README.txt")) . '</p>';
      return $output;
  }
}

/**
 * Get Facebook Application IDs.
 * 
 * @return array
 *   array of facebook Application ids
 */
function socialmedia_importer_facebook_get_applications_id_list() {
  $provider = 'facebook';
  $result = db_query('SELECT id FROM {socialmedia_applications} smia WHERE smia.provider = :provider', array(':provider' => $provider))->fetchAllAssoc('id');
  $application_ids = array();
  if (count($result) > 0) {
    foreach ($result as $application_id) {
      $application_ids[] = $application_id->id;
    }
  }
  return $application_ids;
}
