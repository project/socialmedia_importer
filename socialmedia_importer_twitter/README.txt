CONTENTS OF THIS FILE
---------------------

  * Summary
  * Requirements
  * Twitter Configuration


SUMMARY
-------

Twitter Importer is a developer only module that integrate Twitter´s 
API in Drupal. This module allows you to authenticate with this API and
use this authentication to Import Twitter Data in JSON.


REQUIREMENTS
------------
1. Social Media Importer Module - This modules depends on Social Media Importer
 Module.
  
   
TWITTER CONFIGURATION
---------------------
1. Visit https://dev.twitter.com/apps

2. Create a new Application with appropriate details,
   if you don't have an Application created.
   
3. Add "http://example.com/socialmedia_importer/response_handler" 
   in Callback URL

4. Copy the Consumer key, Consumer secret
   to add Application form of the module.
